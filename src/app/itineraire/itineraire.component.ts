import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-itineraire',
  templateUrl: './itineraire.component.html',
  styleUrls: ['./itineraire.component.css']
})
export class ItineraireComponent implements OnInit {

  tabItin: any[] = [];

  constructor() { }

  ngOnInit(): void {
    // Appel API get itineraires
    this.tabItin.push({nom: 'Balade1', diff: 'Facile'});
    this.tabItin.push({nom: 'Balade2', diff: 'Moyen'});
    this.tabItin.push({nom: 'Balade3', diff: 'Difficile'});
  }

}

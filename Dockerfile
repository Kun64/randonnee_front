FROM node:12.18.3-alpine AS builder
COPY . ./randonnee_front
WORKDIR /randonnee_front
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /randonnee_front/dist/randonnee_front/ /usr/share/nginx/html

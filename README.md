# randonnee_front
BUILD
```
docker build --rm -t randonnee_front:latest .
```
RUN
```
docker run --rm -d -p 90:80/tcp randonnee_front:latest
```
RUNNING
```
docker ps
```
STOP
```
docker remove [id]
```
